SRC = $(wildcard */*.c) $(wildcard *.c)
OBJS = $(SRC:.c=.o)
AOUT = main

CC = gcc
CFLAGS = -Wall -g
FRAMEWORKS = -framework OpenGL -framework SDL2 -framework SDL2_image -framework Cocoa
LDFLAGS =

all : $(AOUT)

run:
	./main

main : $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS) $(FRAMEWORKS)

%.o : %.c
	$(CC) -o $@ -c $< $(CFLAGS) $(FRAMEWORKS)

clean :
	@rm *.o */*.o

cleaner : clean
	@rm $(AOUT)
