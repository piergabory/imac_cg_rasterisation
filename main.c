#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>

#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLKit/GLKMatrix4.h>

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define CIRCLE_SEGMENTS 64
#define TAU 6.283185307

static unsigned int WINDOW_WIDTH = 400;
static unsigned int WINDOW_HEIGHT = 400;

static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;

void updateViewport() {
    float vw,vh;
    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    //garde les proportions correctes si la fenêtre est redimentionnée, (type css contain)
    if (WINDOW_WIDTH > WINDOW_HEIGHT) {
        vw = WINDOW_WIDTH/(float)WINDOW_HEIGHT;
        vh = 1.0;
    } else {
        vw = 1.0;
        vh = WINDOW_HEIGHT/(float)WINDOW_WIDTH;
    }
    
    // gluOrtho2D est déprécié, il faut utiliser cela à la place.
    GLKMatrix4 orthoMat = GLKMatrix4MakeOrtho(-vw, vw, -vh, vh, -1.0f, 1.0f);
    glLoadMatrixf(orthoMat.m);
    
}

const char* filename = "logo_imac_400x400.jpg";

GLuint getSquareList();
GLuint getCircleList();

int main(int argc, char** argv) {
    
    // Initialisation de la SDL
    if(-1 == SDL_Init(SDL_INIT_VIDEO)) {
        fprintf(stderr, "Impossible d'initialiser la SDL. Fin du programme.\n");
        return EXIT_FAILURE;
    }

    // création de la fenêtre
    SDL_Window *window = SDL_CreateWindow(
        "TP OpenGL IMAC",  //titre
                                          
        // position à l'écran
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,

        // dimentions par défault
        WINDOW_WIDTH,
        WINDOW_HEIGHT,

        // Fullscreen ou Flottant (resizable)
        SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
    
    if(window == NULL) {
        fprintf(stderr, "Impossible d'ouvrir la fenetre. Fin du programme.\n");
        return EXIT_FAILURE;
    }
    
    SDL_GLContext glcontext = SDL_GL_CreateContext(window);

    // get lists
    GLuint circle = getCircleList();
    GLuint square = getSquareList();
    
    //time
    time_t rawtime;
    struct tm * timeinfo;
    
    //Boucle de dessin
    int loop = 1;
    glClearColor(0.1, 0.1, 0.1 ,1.0);
    while(loop) {
        Uint32 startTime = SDL_GetTicks();
        glClear(GL_COLOR_BUFFER_BIT);
        
        //get time
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        
        // Dessin
        // save original state
        glPushMatrix();
        glScalef(.9, .9, 1.);
        
        glPushMatrix();
        glColor3f(1.,1.,1.);
        glCallList(circle);
        
        glColor3f(0.,0.,0.);
        glScalef(.95,.95,1.);
        glCallList(circle);
        
        glColor3f(1.,1.,1.);
        glScalef(.05,.05,1.);
        glCallList(circle);
        glPopMatrix();
        
        glPushMatrix();
        for (int mark = 0; mark < 60; mark++) {
            glRotatef(6,0.,0.,1.);
            glPushMatrix();
            glTranslatef(0,.95,0);
            glScalef(.01, 0.05, 1);
            glCallList(square);
            glPopMatrix();
        }
        glPopMatrix();
        
        glScalef(.6,.6,1);
        // seconds
        glPushMatrix();
        glRotatef(timeinfo->tm_sec*6,0.,0.,-1.);
        glScalef(0.01,.8, 1);
        glTranslatef(0,.8,0);
        glCallList(square);
        glPopMatrix();
        
        
        // minutes
        glPushMatrix();
        glRotatef(timeinfo->tm_min*6,0.,0.,-1.);
        glScalef(0.02, .7, 1);
        glTranslatef(0,1.,0);
        glCallList(square);
        glPopMatrix();
        
        // hours
        glPushMatrix();
        glRotatef(timeinfo->tm_hour*30,0.,0.,-1.);
        glScalef(0.03, .5, 1);
        glTranslatef(0,1.,0);
        glCallList(square);
        glPopMatrix();
        
        // restore original state
        glPopMatrix();
        
        
        // Gestion des événements
        SDL_Event e;
        while(SDL_PollEvent(&e)) {

            switch(e.type) {

                case SDL_QUIT:
                    loop = 0;
                    break;

                case SDL_WINDOWEVENT:
                    if(e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED){
                        WINDOW_WIDTH = e.window.data1;
                        WINDOW_HEIGHT = e.window.data2;
                        updateViewport();
                    }
                    break;

                default: break;
            }
        }

        SDL_GL_SwapWindow(window);
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        if(elapsedTime < FRAMERATE_MILLISECONDS) {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    // Liberation des ressources associÃ©es Ã  la SDL
    SDL_Quit();

    return EXIT_SUCCESS;
}

GLuint getCircleList() {
    GLuint id = glGenLists(1);
    
    //init List
    glNewList(id, GL_COMPILE);
    
    // start drawing
    glBegin(GL_POLYGON);
    
    //draw segments
    for (double teta = 0; teta < TAU; teta += TAU/CIRCLE_SEGMENTS)
        glVertex2f(cos(teta), sin(teta));
    
    // end drawing
    glEnd();
    
    // save list
    glEndList();
    
    return id;
}

GLuint getSquareList() {
    GLuint id = glGenLists(1);
    
    //init List
    glNewList(id, GL_COMPILE);
    
    // start drawing
    glBegin(GL_QUADS);
    
    //draw segments
    glVertex2f(1,1);
    glVertex2f(1,-1);
    glVertex2f(-1,-1);
    glVertex2f(-1,1);
    
    // end drawing
    glEnd();
    
    // save list
    glEndList();
    
    return id;
}
